/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cumulations.andoridthingspulsesensor;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.google.android.things.pio.PeripheralManagerService;
import com.google.android.things.pio.SpiDevice;

import java.io.IOException;
import java.util.List;

/**
 * This Activity finds the SPI buslist in the board and connects to the first SPI device.
 * The board we have used this activity is Raspberry Pi .
 *
 * The board comes with a MCP3008 ADC for helping to convert Analog values from pulsesensor
 *
 *  *
 *
 */
public class MainActivity extends Activity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int INTERVAL_BETWEEN_BLINKS_MS = 100;

    private Handler mHandler = new Handler();
    private SpiDevice spiDevice;

    pulseSensorUtil pulseSensor=new pulseSensorUtil();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "Starting MainActivity");

        PeripheralManagerService service = new PeripheralManagerService();
        List<String> spiBusList = service.getSpiBusList();
        Log.i(TAG, "SPI Bus List in the device "+spiBusList.size());

        if (spiBusList.size() <=0)
        {
            Log.i(TAG, "Sorry your device does not support SPI");
            return;
        }

        try {

            Log.i(TAG, "Open SPI Device");
            spiDevice = service.openSpiDevice(spiBusList.get(0));
            Log.i(TAG, "SPI Device configuration");
            configureSpiDevice(spiDevice);
            mHandler.post(deviceReadThread);
        } catch (IOException e) {
            Log.e(TAG, "Error on PeripheralIO API", e);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Remove pending blink Runnable from the handler.
        mHandler.removeCallbacks(deviceReadThread);
        // Close the Gpio pin.


            if (spiDevice != null) {
                try {
                    spiDevice.close();
                    spiDevice = null;
                } catch (IOException e) {
                    Log.w(TAG, "Unable to close SPI device", e);
                }
             finally {

                }
                  }
    }


    /* By default we have selected channel 0 in the ADC
    * Before reading data we need to write about the channel which we will be reading from.
    * */
    private Runnable deviceReadThread = new Runnable() {
        @Override
        public void run() {
            // Exit Runnable if the GPIO is already closed
            if (spiDevice == null) {
                return;
            }
            try {

                Log.i(TAG,"Reading from the SPI");

                byte[] data=new byte[3];
                byte[] response=new byte[3];
                data[0]=1;
                int a2dChannel=0;
                data[1]= (byte) (8 << 4);
                data[2] = 0;

                //full duplex mode
                spiDevice.transfer(data,response,3);

                int a2dVal = 0;
                a2dVal = (response[1]<< 8) & 0b1100000000; //merge data[1] & data[2] to get result
                a2dVal |=  (response[2] & 0xff);


                pulseSensor.process(a2dVal);

                mHandler.postDelayed(deviceReadThread, INTERVAL_BETWEEN_BLINKS_MS);
            } catch (IOException e) {
                Log.e(TAG, "Error on PeripheralIO API", e);
            }
        }
    };



    public void configureSpiDevice(SpiDevice device) throws IOException {
        // Low clock, leading edge transfer
        device.setMode(SpiDevice.MODE0);
        device.setBitsPerWord(8);          // 8 BPW
        device.setFrequency(1000000);     // 1MHz
        device.setBitJustification(false); // MSB first
    }


}
