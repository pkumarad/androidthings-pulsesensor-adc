### What is this repository for? ###

* This repository demonstrates how to read PulseSensor data into AndroidThings enabled Raspberry Pi through MCP3008 ADC.

 [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### You would need ## 

 *   Raspberry Pi running AndroidThings
*    Pulsesensor for reading Heart rate
*    MCP3008 as a Analog to Digital Converter
 

### Description ###

Pulse Sensor is a well known heart rate sensor providing analog output. Pulse Sensor is a simple sensor with 3 wires, Positive,Negative and an analog output pin. Raspberry pi has no analog input pins. Hence we will use an Analog to Digital Converter by name MCP3008. MCP3008 is a 10 bit, 8 channel Analog to Digital converter which supports only SPI  (Serial Peripheral Interface).